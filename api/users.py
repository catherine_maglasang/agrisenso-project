import api
from utils import spcall
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)


def user_exists(username):
    response = spcall("user_exists", (username,), )[0][0]
    return response


def generate_auth_token(username, expiration=600):
    s = Serializer(api.app.config['SECRET_KEY'], expires_in=expiration)
    return s.dumps({'id': username})


def verify_auth_token(token):
    s = Serializer(api.app.config['SECRET_KEY'])
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None  # valid token, but expired
    except BadSignature:
        return None  # invalid token
    user = data['id']
    return user
