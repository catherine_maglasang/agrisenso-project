import flask
from flask import jsonify
from utils import MyJSONEncoder
from flask import Flask
from flask.ext.cors import CORS
from utils import spcall, InvalidForm, InvalidRequest, DuplicateRow
from api.users import verify_auth_token
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)

app = Flask(__name__)

app.json_encoder = MyJSONEncoder


# Views

from api import views


# Handlers

@app.errorhandler(InvalidForm)
def handle_invalid_form(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.errorhandler(InvalidRequest)
def handle_invalid_request(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.errorhandler(DuplicateRow)
def handle_duplicate_row(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


CORS(app)


if __name__ == '__main__':
    app.run()
