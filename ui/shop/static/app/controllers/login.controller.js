'use strict';

angular
    .module('estore')
    .controller('loginController', loginController)

function loginController($scope, $location, $state, toaster, AuthService, Base64, ProductApi) {
    var vm = this;

    if (AuthService.isLoggedIn() == true){
        $location.path('/');
    };

    vm.login = function () {

        // initial values
        vm.error = false;
        vm.disabled = true;

        // call login from service
        AuthService.login(vm.loginForm)
            // handle success
            .then(function () {
                vm.loginForm = {};

                $state.go('dashboard');

                toaster.pop({
                    type: 'success',
                    title: 'Success!',
                    body: 'Welcome!',
                    showCloseButton: true,
                    autodismiss: false
                });

            })
            // handle error
            .catch(function () {
                vm.error = true;
                vm.errorMessage = "Invalid username and/or password";
                vm.disabled = false;
                vm.loginForm = {};

                toaster.pop({
                    type: 'error',
                    title: 'Error',
                    body: 'Invalid username/password',
                    showCloseButton: true,
                    autodismiss: false
                });
            });
    };
}
