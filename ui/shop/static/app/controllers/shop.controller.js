mainApp.controller("ShopController", ['$scope', '$http', 'Product', 'Category', 'toaster', function ($scope, $http, Product, Category, toaster) {
    var vm = this;
    vm.product = new Product();
    vm.category = new Category();
    vm.qty = 0;
    vm.isLoggedIn = false;

    vm.products = [];
    vm.categories = [];
    vm.cart = [];
    vm.cartDetails = {};
    vm.cartDetails.total = 0;
    vm.wishlist = [];

    vm.selected = {};

    vm.initialize = function () {
        Product.get(function (data) {
            vm.products = data.entries;
            console.log(vm.products);
        });
        Category.get(function (data) {
            vm.categories = data.entries;
        });
    };

    vm.selectItem = function (item) {
        console.log(item);
        vm.selected = item;
    };

    vm.addToCart = function (item, qty) {
        var i = {};
        i.product = item;
        i.qty = qty;
        vm.cart.push(i);

        vm.cartDetails.total += item.unit_selling_cost;

        toaster.pop({
            type: 'success',
            title: 'Success',
            body: 'Item added to cart',
            showCloseButton: true
        });

        vm.qty = 0;
    };

    // vm.addToWishlist = function (item, qty) {
    //     var i = {};
    //     i.product = item;
    //     i.qty = qty;
    //     vm.wishlist.push(i);

    //     toaster.pop({
    //         type: 'success',
    //         title: 'Success',
    //         body: 'Item added to wishlist',
    //         showCloseButton: true
    //     });

    //     vm.qty = 0;
    // };

    // vm.removeWishlistItem = function(item){
    //     var index = vm.wishlist.indexOf(item);
    //     vm.wishlist.splice(index, 1);

    //     toaster.pop({
    //         type: 'success',
    //         title: 'Success',
    //         body: 'Item removed from wishlist',
    //         showCloseButton: true
    //     });
    // };

    vm.removeCartItem = function(item){
        var index = vm.cart.indexOf(item);
        vm.cart.splice(index, 1);

        toaster.pop({
            type: 'success',
            title: 'Success',
            body: 'Item removed from cart',
            showCloseButton: true
        });
    };

    vm.selectItem = function (item) {
        vm.selected = item;
    };

    vm.plusQty = function () {
        vm.qty++;
    };

    vm.subQty = function () {
        vm.qty--;
    };

    vm.initialize();

    vm.promotions = [{
        'title':'Shop Fresh Products Straight from the Farm',
        'description': 'Don\'t Miss Any Exclusive Products Just For You.',
        'btn_link': '#/shop/catalogue',
        'btn_text': 'Shop Now',
        'is_banner': 'true',
        'primary_image': 'static/img/backgrounds/banner.jpg'
    }];

}]);
