'use strict';

angular
    .module('estore')
    .controller('registerController', registerController);

function registerController($scope, $location, $state, toaster, AuthService) {
    var vm = this;

    if (AuthService.isLoggedIn() == true){
        $state.go('dashboard');
    };

    vm.registerUser = function () {

        // initial values
        vm.error = false;
        vm.disabled = true;

        // call register from service
        vm.registerForm.user_id = null;
        vm.registerForm.is_admin = true;

        AuthService.register(vm.registerForm)
            // handle success
            .then(function () {
                $state.go('login');
                vm.disabled = false;
                vm.registerForm = {};

                 toaster.pop({
                        type: 'primary',
                        title: 'Success!',
                        body: 'Login to your account!',
                        showCloseButton: true,
                        autodismiss: false
                    });

            })
            // handle error
            .catch(function () {
                vm.error = true;
                vm.errorMessage = "Something went wrong!";
                vm.disabled = false;
                vm.registerForm = {};
            });

    };

};
