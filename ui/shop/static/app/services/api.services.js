mainApp.factory("Product", ['$resource',
    function ($resource) {
        return $resource("http://localhost:5000/products/:id", {id: '@id'}, {});
    }]);

mainApp.factory("Category", ['$resource',
    function ($resource) {
        return $resource("http://localhost:5000/categories/:id", {id: '@id'}, {});
    }]);