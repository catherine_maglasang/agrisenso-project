mainApp.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/shop/');
    $stateProvider
        .state('shop', {
            url: '/shop',
            templateUrl: 'pages/common/content.html',
            controller: 'ShopController',
            controllerAs: 'vm'
        })
        .state('shop.home', {
            url: '/',
            templateUrl: 'pages/home.html'
        })
        .state('shop.cart', {
            url: '/cart',
            templateUrl: 'pages/cart.html'
        })
        .state('shop.checkout', {
            url: '/checkout',
            templateUrl: 'pages/checkout.html'
        })
        .state('shop.catalogue', {
            url: '/catalogue',
            templateUrl: 'pages/category.html'
        })
        .state('shop.category', {
            url: '/catalogue/:id',
            templateUrl: 'pages/category.html'
        })
        .state('shop.productDetail', {
            url: '/product/:id',
            templateUrl: 'pages/product.html'
        })
        .state('shop.login', {
            url: '/login',
            templateUrl: 'pages/login.html',
        })
        .state('shop.register', {
            url: '/register',
            templateUrl: 'pages/register.html'
        })
        .state('shop.password_reset', {
            url: '/password/reset',
            templateUrl: 'pages/password_reset.html'
        })

        .state('shop.profile', {
            url: '/profile',
            templateUrl: 'pages/profile.html'
        })

        // .state('seller', {
        //     url: '/seller',
        //     templateUrl: 'pages/seller/content.html'
        // })

        .state('slogin', {
            url: '/slogin',
            templateUrl: 'pages/seller/login_seller.html'
        })

        .state('sregister', {
            url: '/register',
            templateUrl: 'pages/seller/register_seller.html'
        })

}]);