CREATE TABLE products (
  product_id        SERIAL NOT NULL PRIMARY KEY,
  merchant_id       INT,
  thumb_url         text, 
  title             TEXT,
  long_description  TEXT,
  sku               TEXT,
  short_description TEXT,
  stock_on_hand     NUMERIC,
  unit_selling_cost NUMERIC,
  category_id       INT,
  date_added        TIMESTAMP DEFAULT now(),
  is_active         BOOL      DEFAULT TRUE,
  is_featured       BOOL      DEFAULT TRUE
);


CREATE TABLE product_categories (
  product_category_id SERIAL NOT NULL PRIMARY KEY,
  name                TEXT,
  slug                TEXT,
  parent              INT,
  description         TEXT,
  is_active           BOOL
);

CREATE TABLE tags (
  tag_id SERIAL NOT NULL PRIMARY KEY,
  name   TEXT,
  slug   TEXT
);

CREATE TABLE product_tags (
  product_tag_id SERIAL NOT NULL PRIMARY KEY,
  product_id     INT,
  tag_id         INT
);


CREATE TABLE orders (
  order_id     SERIAL UNIQUE NOT NULL PRIMARY KEY,
  customer_id  INT,
  date_ordered TIMESTAMP,
  status       TEXT,
  reference_no TEXT,
  total_amount NUMERIC
);

CREATE TABLE order_items (
  order_item_id SERIAL PRIMARY KEY,
  order_id      INT REFERENCES orders,
  product_id    INT REFERENCES products,
  quantity      INT
);

CREATE TABLE users (
  user_id      SERIAL       NOT NULL PRIMARY KEY,
  username     VARCHAR(100) NOT NULL UNIQUE,
  email        VARCHAR(100) NOT NULL,
  password     VARCHAR(300) NOT NULL,
  date_created TIMESTAMP DEFAULT now(),
  is_admin     BOOLEAN
);

CREATE TABLE customers (
  customer_id      SERIAL PRIMARY KEY,
  user_id          INT REFERENCES users,
  name             TEXT,
  billing_address  TEXT,
  shipping_address TEXT,
  date_created     TIMESTAMP
);

CREATE TABLE suppliers (
  supplier_id SERIAL UNIQUE NOT NULL PRIMARY KEY,
  name        TEXT,
  address     TEXT,
  phone       TEXT,
  fax         TEXT,
  email       TEXT,
  is_active   BOOLEAN
);

CREATE TABLE attributes (
  attribute_id   SERIAL NOT NULL PRIMARY KEY,
  attribute_name TEXT,
  validation     TEXT
);

CREATE TABLE product_attributes (
  attribute_value TEXT,
  attribute_id    INT,
  product_id      INT
);

CREATE TABLE product_images (
  image_id   SERIAL NOT NULL PRIMARY KEY,
  product_id INT,
  image      TEXT,
  caption    TEXT
);

CREATE TABLE category_images (
  image_id    SERIAL NOT NULL PRIMARY KEY,
  category_id INT,
  image       TEXT,
  caption     TEXT
);


CREATE TABLE carts (
  cart_id      SERIAL NOT NULL PRIMARY KEY,
  session_id   INT,
  date_created TIMESTAMP,
  customer_id  INT,
  is_active    BOOLEAN
);

CREATE TABLE cart_items (
  cart_item_id SERIAL NOT NULL PRIMARY KEY,
  cart_id      INT,
  product_id   INT,
  quantity     INT,
  time_stamp   TIMESTAMP
);
