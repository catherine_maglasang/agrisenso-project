CREATE OR REPLACE FUNCTION cart_items_get(IN par_cart_id INT, IN par_cart_item_id INT)
  RETURNS SETOF cart_items AS $$
BEGIN
  IF par_cart_item_id ISNULL
  THEN
    RETURN QUERY SELECT *
                 FROM cart_items
                 WHERE cart_id = par_cart_id;
  ELSE
    RETURN QUERY SELECT *
                 FROM cart_items
                 WHERE id = par_cart_item_id AND cart_id = par_cart_id;
  END IF;
END;
$$ LANGUAGE 'plpgsql';