
CREATE OR REPLACE FUNCTION attributes_upsert(IN par_attribute_id   INT,
                                             IN par_attribute_name TEXT, IN par_validation TEXT)
  RETURNS TEXT AS $$
DECLARE
  loc_response TEXT;
BEGIN

  IF par_attribute_id ISNULL
  THEN
    INSERT INTO attributes (attribute_name, validation)
    VALUES (par_attribute_name, par_validation);
    loc_response = 'OK';
  ELSE
    UPDATE attributes
    SET attribute_name = par_attribute_name, validation = par_validation
    WHERE attribute_id = par_attribute_id;
    loc_response = 'OK';
  END IF;

  RETURN loc_response;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION tags_upsert(IN par_tag_id INT,
                                       IN par_name   TEXT, IN par_slug TEXT)
  RETURNS TEXT AS $$
DECLARE
  loc_response TEXT;
BEGIN

  IF par_tag_id ISNULL
  THEN
    INSERT INTO tags (name, slug)
    VALUES (par_name, par_slug);
    loc_response = 'OK';
  ELSE
    UPDATE tags
    SET name = par_name, slug = par_slug
    WHERE tag_id = par_tag_id;
    loc_response = 'OK';
  END IF;

  RETURN loc_response;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION product_tags_upsert(IN par_product_id INT,
                                               IN par_tag_id     INT)
  RETURNS TEXT AS $$
DECLARE
  loc_response TEXT;
BEGIN

  IF par_tag_id ISNULL
  THEN
    INSERT INTO product_tags (product_id, tag_id)
    VALUES (par_product_id, par_tag_id);
    loc_response = 'OK';
  ELSE
    UPDATE product_tags
    SET tag_id = par_tag_id
    WHERE tag_id = par_tag_id;
    loc_response = 'OK';
  END IF;

  RETURN loc_response;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION attributes_get(IN par_attribute_id INT)
  RETURNS SETOF attributes AS $$
BEGIN
  IF par_attribute_id ISNULL
  THEN
    RETURN QUERY SELECT *
                 FROM attributes;
  ELSE
    RETURN QUERY SELECT *
                 FROM attributes
                 WHERE attribute_id = par_attribute_id;
  END IF;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION tags_get(IN par_tag_id INT)
  RETURNS SETOF tags AS $$
BEGIN
  IF par_tag_id ISNULL
  THEN
    RETURN QUERY SELECT *
                 FROM tags;
  ELSE
    RETURN QUERY SELECT *
                 FROM tags
                 WHERE tag_id = par_tag_id;
  END IF;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION product_tags_get(IN par_product_id INT, IN par_tag_id INT)
  RETURNS SETOF product_tags AS $$
BEGIN
  IF par_tag_id ISNULL
  THEN
    RETURN QUERY SELECT *
                 FROM product_tags
                 WHERE product_id = par_product_id;
  ELSE
    RETURN QUERY SELECT *
                 FROM product_tags
                 WHERE tag_id = par_tag_id AND product_id = par_product_id;
  END IF;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION new_user(IN par_username     TEXT, IN par_email TEXT, IN par_password TEXT,
                                    IN par_date_created TIMESTAMP, IN par_is_admin BOOLEAN)
  RETURNS TEXT AS
$$
DECLARE
  loc_id   TEXT;
  loc_res  TEXT;
  loc_user TEXT;
BEGIN
  SELECT INTO loc_user username
  FROM users
  WHERE username = par_username;
  IF loc_user ISNULL
  THEN
    IF par_username = '' OR par_password = '' OR par_email = ''
    THEN
      loc_res='error';
    ELSE
      INSERT INTO users (username, email, password, date_created, is_admin)
      VALUES (par_username, par_email, par_password, par_date_created, par_is_admin);
      loc_res = 'OK';
    END IF;
  ELSE
    loc_res = 'USER EXISTS';
  END IF;
  RETURN loc_res;
END;
$$
LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION categories_upsert(IN par_product_category_id INT,
                                             IN par_name                TEXT, IN par_slug TEXT, par_parent INT,
                                                par_description         TEXT, par_is_active BOOLEAN)
  RETURNS TEXT AS $$
DECLARE
  loc_response TEXT;
BEGIN

  IF par_product_category_id ISNULL
  THEN
    INSERT INTO product_categories (name, slug, parent, description, is_active)
    VALUES (par_name, par_slug, par_parent, par_description, par_is_active);
    loc_response = 'OK';
  ELSE
    UPDATE product_categories
    SET name = par_name, slug = par_slug
    WHERE product_category_id = par_product_category_id;
    loc_response = 'OK';
  END IF;

  RETURN loc_response;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION categories_get(IN par_product_category_id INT)
  RETURNS SETOF product_categories AS $$
BEGIN
  IF par_product_category_id ISNULL
  THEN
    RETURN QUERY SELECT *
                 FROM product_categories;
  ELSE
    RETURN QUERY SELECT *
                 FROM product_categories
                 WHERE product_category_id = par_product_category_id;
  END IF;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION products_in_categories_get(IN par_product_category_id INT, IN par_product_id INT)
  RETURNS SETOF products AS $$
BEGIN
  IF par_product_id ISNULL
  THEN
    RETURN QUERY SELECT *
                 FROM products
                 WHERE category_id = par_product_category_id;
  ELSE
    RETURN QUERY SELECT *
                 FROM products
                 WHERE category_id = par_product_category_id AND product_id = par_product_id;
  END IF;
END;
$$ LANGUAGE 'plpgsql';

--
--
--
CREATE OR REPLACE FUNCTION users_get(IN par_user_id INT)
  RETURNS SETOF users AS $$
BEGIN
  IF par_user_id ISNULL
  THEN
    RETURN QUERY SELECT *
                 FROM users;
  ELSE
    RETURN QUERY SELECT *
                 FROM users
                 WHERE user_id = par_user_id;
  END IF;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION users_username_get(IN par_user_id VARCHAR(100))
  RETURNS SETOF users AS $$
BEGIN
  IF par_user_id ISNULL
  THEN
    RETURN QUERY SELECT *
                 FROM users;
  ELSE
    RETURN QUERY SELECT *
                 FROM users
                 WHERE username = par_user_id;
  END IF;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION users_upsert(IN par_user_id  INT, IN par_username VARCHAR(100), IN par_email VARCHAR(100),
                                        IN par_password VARCHAR(300), IN par_date_created TIMESTAMP,
                                        IN par_is_admin BOOLEAN)
  RETURNS TEXT AS $$
DECLARE
  loc_response TEXT;
BEGIN

  IF par_user_id ISNULL
  THEN
    INSERT INTO users (username, email, password, date_created, is_admin)
    VALUES (par_username, par_email, par_password, par_date_created, par_is_admin)
    RETURNING user_id
      INTO loc_response;
  ELSE
    UPDATE users
    SET username = par_username
    WHERE user_id = par_user_id;
    loc_response = 'OK';
  END IF;

  RETURN loc_response;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION suppliers_new(IN par_name TEXT, IN par_address TEXT, IN par_phone TEXT,
                                         IN par_fax  TEXT, IN par_email TEXT, IN par_is_active BOOLEAN)
  RETURNS TEXT AS
$$
DECLARE
  loc_id  TEXT;
  loc_res TEXT;
BEGIN

  INSERT INTO suppliers (name, address, phone, fax, email, is_active)
  VALUES (par_name, par_address, par_phone, par_fax, par_email, par_is_active);
  loc_res = 'OK';

  RETURN loc_res;
END;
$$
LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION suppliers_get(IN par_supplier_id INT)
  RETURNS SETOF suppliers AS
$$
BEGIN
  IF par_supplier_id ISNULL
  THEN
    RETURN QUERY SELECT *
                 FROM suppliers;
  ELSE
    RETURN QUERY SELECT *
                 FROM suppliers
                 WHERE supplier_id = par_supplier_id;
  END IF;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION carts_new(IN par_session_id  INT, IN par_date_created DATE,
                                     IN par_customer_id INT, IN par_is_active BOOLEAN)
  RETURNS TEXT AS
$$
DECLARE
  loc_id  TEXT;
  loc_res TEXT;
BEGIN
  INSERT INTO carts (session_id, date_created, customer_id, is_active)
  VALUES (par_session_id, par_date_created, par_customer_id, par_is_active);
  loc_res = 'ok';
  RETURN loc_res;
END;
$$
LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION carts_get(IN par_cart_id INT)
  RETURNS SETOF carts AS $$
BEGIN
  IF par_cart_id ISNULL
  THEN
    RETURN QUERY SELECT *
                 FROM carts;
  ELSE
    RETURN QUERY SELECT *
                 FROM carts
                 WHERE cart_id = par_cart_id;
  END IF;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION cart_items_new(IN par_cart_id  INT, IN par_product_id INT,
                                          IN par_quantity INT, IN par_time_stamp TIMESTAMP)
  RETURNS TEXT AS
$$
DECLARE
  loc_id  TEXT;
  loc_res TEXT;
BEGIN

  INSERT INTO cart_items (cart_id, product_id, quantity, time_stamp)
  VALUES (par_cart_id, par_product_id, par_quantity, par_time_stamp);
  loc_res = 'OK';

  RETURN loc_res;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION cart_items_get(IN par_cart_id INT, IN par_item_id INT)
  RETURNS SETOF cart_items AS $$
BEGIN
  IF par_item_id ISNULL
  THEN
    RETURN QUERY SELECT *
                 FROM cart_items
                 WHERE cart_id = par_cart_id;
  ELSE
    RETURN QUERY SELECT *
                 FROM cart_items
                 WHERE product_id = par_item_id AND cart_id = par_cart_id;
  END IF;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION orders_new(IN par_customer_id INT, IN par_date_ordered TIMESTAMP,
                                      IN par_status      TEXT, IN par_reference_no TEXT, IN par_total_amount NUMERIC)
  RETURNS TEXT AS
$$
DECLARE
  loc_id  TEXT;
  loc_res TEXT;
BEGIN

  INSERT INTO orders (customer_id, date_ordered, status, reference_no, total_amount)
  VALUES (par_customer_id, par_date_ordered, par_status, par_reference_no, par_total_amount);

  loc_res = 'OK';

  RETURN loc_res;
END;
$$

LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION orders_get(IN par_order_id INT)
  RETURNS SETOF orders AS
$$
BEGIN
  IF par_order_id ISNULL
  THEN
    RETURN QUERY SELECT *
                 FROM orders;
  ELSE
    RETURN QUERY SELECT *
                 FROM orders
                 WHERE order_id = par_order_id;

  END IF;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION order_items_new(IN par_order_id INT, IN par_item_id INT,
                                           IN par_quantity INT)
  RETURNS TEXT AS
$$
DECLARE
  loc_id  TEXT;
  loc_res TEXT;
BEGIN
  INSERT INTO order_items (order_id, product_id, quantity)
  VALUES (par_order_id, par_item_id, par_quantity);
  loc_res = 'OK';
  RETURN loc_res;
END;
$$

LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION order_items_get(IN par_order_id INT, IN par_product_id INT)
  RETURNS SETOF order_items AS
$$
BEGIN
  IF par_product_id ISNULL
  THEN
    RETURN QUERY SELECT *
                 FROM order_items
                 WHERE order_id = par_order_id;
  ELSE
    RETURN QUERY SELECT *
                 FROM order_items
                 WHERE product_id = par_product_id AND order_id = par_order_id;
  END IF;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION new_customer(IN par_id               INT, IN par_first_name TEXT, IN par_last_name TEXT,
                                        IN par_address          TEXT, IN par_city TEXT, IN par_state TEXT,
                                        IN par_postal_code      TEXT, IN par_country TEXT, IN par_phone TEXT,
                                        IN par_email            TEXT, IN par_user_id INT, IN par_billing_address TEXT,
                                        IN par_shipping_address TEXT, IN par_date_created TIMESTAMP)
  RETURNS TEXT AS
$$
DECLARE
  loc_id  TEXT;
  loc_res TEXT;
BEGIN
  SELECT INTO loc_id customer_id
  FROM customers
  WHERE customer_id = par_id;
  IF loc_id ISNULL
  THEN
    IF par_first_name = '' OR par_last_name = '' OR par_address = '' OR par_city = '' OR par_state = '' OR
       par_postal_code = '' OR par_country = '' OR par_phone = '' OR par_email = '' OR par_billing_address = '' OR
       par_shipping_address = ''
    THEN
      loc_res='error';
    ELSE
      INSERT INTO customers (customer_id, first_name, last_name, address, city, state, postal_code, country, phone, email, user_id, billing_address, shipping_address, date_created)
      VALUES (par_id, par_first_name, par_last_name, par_address, par_city, par_state, par_postal_code, par_country,
                      par_phone, par_email, par_user_id, par_billing_address, par_shipping_address, par_date_created);
      loc_res = 'OK';
    END IF;
  ELSE
    loc_res = 'CUSTOMER EXISTS';
  END IF;
  RETURN loc_res;
END;
$$

LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION customers_get(IN par_customer_id INT)
  RETURNS SETOF customers AS
$$
BEGIN
  IF par_customer_id ISNULL
  THEN
    RETURN QUERY SELECT *
                 FROM customers;
  ELSE
    RETURN QUERY SELECT *
                 FROM customers
                 WHERE customer_id = par_customer_id;
  END IF;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION products_get(IN par_product_id INT)
  RETURNS SETOF products AS $$
BEGIN
  IF par_product_id ISNULL
  THEN
    RETURN QUERY SELECT *
                 FROM products;
  ELSE
    RETURN QUERY SELECT *
                 FROM products
                 WHERE product_id = par_product_id;
  END IF;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION product_images_get(IN par_image_id INT)
  RETURNS SETOF product_images AS $$
BEGIN
  IF par_image_id ISNULL
  THEN
    RETURN QUERY SELECT *
                 FROM product_images;
  ELSE
    RETURN QUERY SELECT *
                 FROM product_images
                 WHERE image_id = par_image_id;
  END IF;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION category_images_get(IN par_image_id INT)
  RETURNS SETOF category_images AS $$
BEGIN
  IF par_image_id ISNULL
  THEN
    RETURN QUERY SELECT *
                 FROM category_images;
  ELSE
    RETURN QUERY SELECT *
                 FROM category_images
                 WHERE image_id = par_image_id;
  END IF;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION product_attributes_get(IN par_attribute_id INT, IN par_product_id INT)
  RETURNS SETOF product_attributes AS $$
BEGIN
  IF par_attribute_id ISNULL
  THEN
    RETURN QUERY SELECT *
                 FROM product_attributes
                 WHERE product_id = par_product_id;
  ELSE
    RETURN QUERY SELECT *
                 FROM product_attributes
                 WHERE attribute_id = par_attribute_id AND product_id = par_product_id;
  END IF;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION products_upsert(IN par_product_id    INT, in par_merchant_id int, in par_thumb_url text, IN par_title TEXT, IN par_long_description TEXT,
                                           IN par_sku           TEXT, IN par_short_description TEXT,
                                           IN par_stock_on_hand NUMERIC, IN par_unit_selling_cost NUMERIC,
                                           IN par_category_id   INT,
                                           IN par_date_added    TIMESTAMP, IN par_is_active BOOLEAN,
                                           IN par_is_featured   BOOLEAN)
  RETURNS TEXT AS $$
DECLARE
  loc_response TEXT;
  loc_out      TEXT;
BEGIN

  IF par_product_id ISNULL
  THEN
    INSERT INTO products (merchant_id, thumb_url, title, long_description, sku, short_description, stock_on_hand, unit_selling_cost, category_id, is_active, is_featured)
    VALUES (par_merchant_id, par_thumb_url, par_title, par_long_description, par_sku, par_short_description, par_stock_on_hand, par_unit_selling_cost,
            par_category_id,
            par_is_active, par_is_featured)
    RETURNING product_id
      INTO loc_response;
  ELSE

    SELECT INTO loc_out product_id
    FROM products
    WHERE product_id = par_product_id;

    IF loc_out ISNULL
    THEN
      loc_response = 'error';
    ELSE

      UPDATE products
      SET title = par_title, short_description = par_short_description, long_description = par_long_description,
        sku     = par_sku
      WHERE product_id = par_product_id;
      loc_response = par_product_id;

    END IF;

  END IF;

  RETURN loc_response;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION customers_upsert(IN par_customer_id      INT, IN par_user_id INT, IN par_name TEXT,
                                            IN par_billing_address  TEXT,
                                            IN par_shipping_address TEXT, IN par_date_created TIMESTAMP)
  RETURNS TEXT AS $$
DECLARE
  loc_response TEXT;
BEGIN

  IF par_customer_id ISNULL
  THEN
    INSERT INTO customers (user_id, name, billing_address, shipping_address, date_created)
    VALUES (par_user_id, par_name, par_billing_address, par_shipping_address, par_date_created);
    loc_response = 'OK';
  ELSE
    UPDATE customers
    SET name = par_name, billing_address = par_billing_address, shipping_address = par_shipping_address
    WHERE customer_id = par_customer_id;
    loc_response = 'OK';
  END IF;

  RETURN loc_response;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION product_images_upsert(IN par_image_id INT, IN par_product_id INT, IN par_image_url TEXT,
                                                 IN par_caption  TEXT)
  RETURNS TEXT AS $$
DECLARE
  loc_response TEXT;
BEGIN

  IF par_image_id ISNULL
  THEN
    INSERT INTO product_images (product_id, image, caption)
    VALUES (par_product_id, par_image_url, par_caption);
    loc_response = 'OK';
  ELSE
    UPDATE product_images
    SET image = par_image_url, caption = par_caption;
    loc_response = 'OK';
  END IF;

  RETURN loc_response;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION category_images_upsert(IN par_image_id INT, IN par_category_id INT, IN par_image_url TEXT,
                                                  IN par_caption  TEXT)
  RETURNS TEXT AS $$
DECLARE
  loc_response TEXT;
BEGIN

  IF par_image_id ISNULL
  THEN
    INSERT INTO category_images (category_id, image, caption)
    VALUES (par_category_id, par_image_url, par_caption);
    loc_response = 'OK';
  ELSE
    UPDATE category_images
    SET image = par_image_url, caption = par_caption;
    loc_response = 'OK';
  END IF;

  RETURN loc_response;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION product_attributes_upsert(IN par_attribute_value TEXT, IN par_attribute_id INT,
                                                     IN par_product_id      INT)
  RETURNS TEXT AS $$
DECLARE
  loc_response TEXT;
  loc_id       INT;
BEGIN
  SELECT INTO loc_id attribute_id
  FROM product_attributes
  WHERE attribute_id = par_attribute_id AND product_id = par_product_id;
  IF loc_id ISNULL
  THEN
    INSERT INTO product_attributes (attribute_id, product_id, attribute_value)
    VALUES (par_attribute_id, par_product_id, par_attribute_value);
    loc_response = 'OK';
  ELSE
    UPDATE product_attributes
    SET attribute_value = par_attribute_value
    WHERE product_id = par_product_id AND attribute_id = par_attribute_id;
    loc_response = 'OK';
  END IF;

  RETURN loc_response;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION getpassword(IN par_username VARCHAR(100))
  RETURNS VARCHAR AS
$$
DECLARE
  loc_password TEXT;
BEGIN
  SELECT INTO loc_password password
  FROM users
  WHERE username = par_username;
  IF loc_password ISNULL
  THEN
    loc_password = 'null';
  END IF;
  RETURN loc_password;
END;
$$
LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION getusername(IN par_username VARCHAR(100))
  RETURNS VARCHAR AS
$$
DECLARE
  loc_username TEXT;
BEGIN
  SELECT INTO loc_username username
  FROM users
  WHERE username = par_username;
  IF loc_username ISNULL
  THEN
    loc_username = 'null';
  END IF;
  RETURN loc_username;
END;
$$
LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION user_exists(IN par_username VARCHAR(100))
  RETURNS BOOLEAN AS $$
DECLARE
  loc_user TEXT;
  loc_res  BOOLEAN;
BEGIN
  SELECT INTO loc_user username
  FROM users
  WHERE username = par_username;
  IF loc_user ISNULL
  THEN
    loc_res  = FALSE;
  ELSE
    loc_res = TRUE;
  END IF;
  RETURN loc_res;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION users_delete(IN par_username VARCHAR(100))
  RETURNS VOID AS $$
DELETE FROM users
WHERE username = 'test';
$$
LANGUAGE 'sql';
--
--
--
CREATE OR REPLACE FUNCTION supplier_exist(IN par_supplier_name TEXT)
  RETURNS BOOLEAN AS $$
DECLARE
  loc_supplier TEXT;
  loc_res      BOOLEAN;
BEGIN
  SELECT INTO loc_supplier name
  FROM suppliers
  WHERE name = par_supplier_name;

  IF loc_supplier ISNULL
  THEN
    loc_res  = FALSE;
  ELSE
    loc_res = TRUE;
  END IF;

  RETURN loc_res;
END;
$$ LANGUAGE 'plpgsql';
--
--
--
CREATE OR REPLACE FUNCTION suppliers_delete(IN par_supplier_name TEXT)
  RETURNS VOID AS $$
DELETE FROM suppliers
WHERE name = par_supplier_name;
$$
LANGUAGE 'sql';
--
--
--
-- SELECT new_user('ako', 'ako@gmail.com', 'ako', '1/1/1 1:1:1', TRUE);
SELECT categories_upsert(NULL, 'Beans', 'beans', NULL, 'Beans Category', TRUE);
SELECT categories_upsert(NULL, 'Root Crops', 'root crops', NULL, 'Root Crops Category', TRUE);
SELECT categories_upsert(NULL, 'Leaves', 'leaves', NULL, 'Leaves Category', TRUE);
SELECT categories_upsert(NULL, 'Fruits', 'fruits', NULL, 'Fruits Category', TRUE);
SELECT categories_upsert(NULL, 'Rice', 'rice', NULL, 'Rice Category', TRUE);
SELECT categories_upsert(NULL, 'Corn', 'corn', NULL, 'Corn Category', TRUE);