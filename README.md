# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

An ecommerce platform for agricultural products that is based in Mindanao.

Version 1.0


### How do I get set up? ###

Make sure, vagrant is installed.

Follow the ff commands:

git clone project

vagrant up

vagrant provision

vagrant ssh

* Running python API (5000)

python run.py

* Running UI for Shop

python -m SimpleHTTPServer 8085

* Running UI for Seller

python -m SimpleHTTPServer 8080

* Running Test in Lettuce

python lettuce step.py


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

Catherine Maglasang

Marjorie Buctolan


* Other community or team contact

maglasangcatherine12@gmail.com

marjbuctolan@gmail.com


### What to do if IP address is in use ###

$sudo lsof -i tcp:<port#> (5000, 8085, 8080)

$kill -9 <PID>
